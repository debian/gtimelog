"""
Resource locations for running out of source checkouts and pip installs
"""

import os
import subprocess
import sys


here = os.path.dirname(__file__)

ui_dir = '/usr/share/gtimelog'

# XXX: can't check Gtk version here to use fallback 3.10 .ui files!
UI_FILE = os.path.join(ui_dir, 'gtimelog.ui')
PREFERENCES_UI_FILE = os.path.join(ui_dir, 'preferences.ui')
ABOUT_DIALOG_UI_FILE = os.path.join(ui_dir, 'about.ui')
SHORTCUTS_UI_FILE = os.path.join(ui_dir, 'shortcuts.ui')
MENUS_UI_FILE = os.path.join(ui_dir, 'menus.ui')
CSS_FILE = os.path.join(ui_dir, 'gtimelog.css')

LOCALE_DIR = '/usr/share/locale'
CONTRIBUTORS_FILE = os.path.join('/usr/share/doc/gtimelog', 'CONTRIBUTORS.rst')
